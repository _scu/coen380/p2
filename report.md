# Project 2: COEN380 (Advanced Database Systems)

## Team: Group #2

Ankur Dang | Ankush Chaknalwar | Mrithyunjay Halinge | Niral Shah | Sampath Shreekantha
---- | ---- | ---- | ---- | ----
1545115 | 1549325 | 1552192 | 1425877 | 1549494

## Description
IMDb (Internet Movie Database)/Rotten Tomatoes is an online database of information related to films, television programs and streaming content online – including cast, production crew and personal biographies, plot summaries, trivia, fan and critical reviews, and ratings. The database has various tables inluding movies, actors, genres, ratings, directors etc. The database also reduces search with the implementation of indexes. The main goal of this is to show implementation of queries of varied complexity.

## Dataset
[MovieLens + IMDb/Rotten Tomatoes](http://files.grouplens.org/datasets/hetrec2011/hetrec2011-movielens-2k-v2.zip)

This dataset is an extension of MovieLens10M dataset, published by [GroupLeans research group](http://www.grouplens.org).
It links the movies of MovieLens dataset with their corresponding web pages at [Internet Movie Database (IMDb)](http://www.imdb.com) and [Rotten Tomatoes](http://www.rottentomatoes.com) movie review systems.

From the original dataset, only those users with both rating and tagging information have been mantained.
The dataset is released in the framework of the 2nd International Workshop on Information [Heterogeneity and Fusion in Recommender Systems (HetRec 2011)](http://ir.ii.uam.es/hetrec2011) at the 5th ACM Conference on [Recommender Systems (RecSys 2011)](http://recsys.acm.org/2011)

This dataset was built by Iván Cantador with the collaboration of Alejandro Bellogín and Ignacio Fernández-Tobías, members of the Information Retrieval group at [Universidad Autonoma de Madrid](http://ir.ii.uam.es)

|10197 | movies|
|20 | movie genres|
|20809 | movie genre assignments|
| | avg. 2.040 genres per movie|
|4060 | directors|
|95321 | actors|
| | avg. 22.778 actors per movie|
|72 | countries|
|10197 | country assignments|
| | avg. 1.000 countries per movie|
|47899 | location assignments|
| | avg. 5.350 locations per movie|
|855598 | ratings|
| | avg. 404.921 ratings per user|
| | avg. 84.637 ratings per movie|

1. **movies**: It has fields such as title, movieId, year etc it has one to many relationships with movie_countries, movie_actors, user_ratedmovies ,movie_location, movie_directors and movie_genres.
1. **people**: It has fields Id, pname. This table stores list of all the people, who are related to film making life actors, directors, etc. It has one to many relationship with movie_actors.
1. **movie_actors**: It has fields movie_id, actor_id,actor_name and ranking.The table consists of all the roles that an actor can play.
1. **movie_countries**: It has movie_id and country. The table contains the list of all the countries where the production houses of the movies produced reside.
1. **user_ratedmoives**:  It has user_id, movie_id, rating, date, day etc. This table contains ratings given by the user for the respective movies along with its time stamp.
1. **movie_location**: movie_id, location1, location2 etc. This table contains the list of locations where all scenes of the movies have been shot.
1. **movie_directors**: It has movie_id, director_id and director_name. The table contains list of all the directors associated with all the moves on the database.
1. **movie_genres**: movie_id and genre. This table contains list of all the genres associate with the movies.

## Implementation
We are using mysql 8.0.17 and mysql jdbc 8.0.17 connector for implementation of our project.
We have set up a mysql server in our personal laptop with the following configuration 

OS | MacOS 10.15 – Catalina
CPU Frequency | 2.2GHz
Memory | 16GB RAM
CPU | 6 core intel 17

The mySQL server setup with the following address – locolhost:3306/P2. Which implies that a TCP (Transmission Control Protocol) port 3306 is opened up for JDBC connection through which the java client can communicate with the mySQL server.

We are using batchUpdate() Java API for populating the data into the tables. Submits a batch of commands to the database for execution and if all commands execute successfully, returns an array of update counts.The batch size used in our implementation is `50000`. This helped us to improve the performance by reducing the number of JDBC calls and batching the data.

## EER Diagram

![EER Diagram](images/EER diagram.png)

## Schema
```sql
mysql> DESC movies;
```
#### Console Output
```
+------------------------+--------------+------+-----+---------+-------+
| Field                  | Type         | Null | Key | Default | Extra |
+------------------------+--------------+------+-----+---------+-------+
| movieID                | int(11)      | NO   | PRI | NULL    |       |
| title                  | varchar(250) | NO   |     | NULL    |       |
| imdbID                 | varchar(7)   | NO   |     | NULL    |       |
| spanishTitle           | varchar(250) | YES  |     | NULL    |       |
| imdbPictureURL         | varchar(300) | YES  |     | NULL    |       |
| year                   | smallint(6)  | NO   | MUL | NULL    |       |
| rtID                   | varchar(100) | YES  |     | NULL    |       |
| rtAllCriticsRating     | smallint(6)  | YES  |     | NULL    |       |
| rtAllCriticsNumReviews | smallint(6)  | YES  |     | NULL    |       |
| rtAllCriticsNumFresh   | smallint(6)  | YES  |     | NULL    |       |
| rtAllCriticsNumRotten  | smallint(6)  | YES  |     | NULL    |       |
| rtAllCriticsScore      | smallint(6)  | YES  |     | NULL    |       |
| rtTopCriticsRating     | int(11)      | YES  |     | NULL    |       |
| rtTopCriticsNumReviews | smallint(6)  | YES  |     | NULL    |       |
| rtTopCriticsNumFresh   | smallint(6)  | YES  |     | NULL    |       |
| rtTopCriticsNumRotten  | smallint(6)  | YES  |     | NULL    |       |
| rtTopCriticsScore      | smallint(6)  | YES  |     | NULL    |       |
| rtAudienceRating       | int(11)      | YES  |     | NULL    |       |
| rtAudienceNumRatings   | int(11)      | YES  |     | NULL    |       |
| rtAudienceScore        | smallint(6)  | YES  |     | NULL    |       |
| rtPictureURL           | varchar(300) | YES  |     | NULL    |       |
+------------------------+--------------+------+-----+---------+-------+
21 rows in set (0.02 sec)
```
```sql
mysql> DESC people;
```
#### Console Output
```
+-------+-------------+------+-----+---------+-------+
| Field | Type        | Null | Key | Default | Extra |
+-------+-------------+------+-----+---------+-------+
| ID    | varchar(60) | NO   | PRI | NULL    |       |
| pname | varchar(60) | YES  |     | NULL    |       |
+-------+-------------+------+-----+---------+-------+
2 rows in set (0.00 sec)
```
```sql
mysql> DESC movie_actors;
```
#### Console Output
```
+-----------+-------------+------+-----+---------+-------+
| Field     | Type        | Null | Key | Default | Extra |
+-----------+-------------+------+-----+---------+-------+
| movieID   | int(11)     | NO   | PRI | NULL    |       |
| actorID   | varchar(60) | NO   | PRI | NULL    |       |
| actorName | varchar(60) | NO   | PRI | NULL    |       |
| ranking   | smallint(6) | NO   |     | NULL    |       |
+-----------+-------------+------+-----+---------+-------+
4 rows in set (0.00 sec)
```
```sql
mysql> DESC movie_directors;
```
#### Console Output
```
+--------------+-------------+------+-----+---------+-------+
| Field        | Type        | Null | Key | Default | Extra |
+--------------+-------------+------+-----+---------+-------+
| movieID      | int(11)     | NO   | PRI | NULL    |       |
| directorID   | varchar(60) | NO   | PRI | NULL    |       |
| directorName | varchar(60) | YES  |     | NULL    |       |
+--------------+-------------+------+-----+---------+-------+
3 rows in set (0.00 sec)
```
```sql
mysql> DESC movie_genres;
```
#### Console Output
```
+---------+-------------+------+-----+---------+-------+
| Field   | Type        | Null | Key | Default | Extra |
+---------+-------------+------+-----+---------+-------+
| movieID | int(11)     | NO   | MUL | NULL    |       |
| genre   | varchar(60) | NO   | MUL | NULL    |       |
+---------+-------------+------+-----+---------+-------+
2 rows in set (0.00 sec)
```
```sql
mysql> DESC movie_countries;
```
#### Console Output
```
+---------+-------------+------+-----+---------+-------+
| Field   | Type        | Null | Key | Default | Extra |
+---------+-------------+------+-----+---------+-------+
| movieID | int(11)     | NO   | MUL | NULL    |       |
| country | varchar(50) | YES  | MUL | NULL    |       |
+---------+-------------+------+-----+---------+-------+
2 rows in set (0.00 sec)
```
```sql
mysql> DESC movie_locations;
```
#### Console Output
```
+-----------+--------------+------+-----+---------+-------+
| Field     | Type         | Null | Key | Default | Extra |
+-----------+--------------+------+-----+---------+-------+
| movieID   | int(11)      | NO   | MUL | NULL    |       |
| location1 | varchar(25)  | YES  |     | NULL    |       |
| location2 | varchar(50)  | YES  |     | NULL    |       |
| location3 | varchar(100) | YES  |     | NULL    |       |
| location4 | varchar(200) | YES  |     | NULL    |       |
+-----------+--------------+------+-----+---------+-------+
5 rows in set (0.01 sec)
```
```sql
mysql> DESC user_ratedmovies;
```
#### Console Output
```
+-------------+-------------+------+-----+---------+-------+
| Field       | Type        | Null | Key | Default | Extra |
+-------------+-------------+------+-----+---------+-------+
| userID      | int(11)     | NO   |     | NULL    |       |
| movieID     | int(11)     | NO   | MUL | NULL    |       |
| rating      | smallint(6) | NO   |     | NULL    |       |
| date_day    | smallint(6) | NO   |     | NULL    |       |
| date_month  | smallint(6) | NO   |     | NULL    |       |
| date_year   | smallint(6) | NO   |     | NULL    |       |
| date_hour   | smallint(6) | NO   |     | NULL    |       |
| date_minute | smallint(6) | NO   |     | NULL    |       |
| date_second | smallint(6) | NO   |     | NULL    |       |
+-------------+-------------+------+-----+---------+-------+
9 rows in set (0.01 sec)
```

## Insert/Update/Delete

### This query creates table of movie_actors.

```sql
mysql> CREATE TABLE people(
    ->     ID INT NOT NULL,
    ->     pname VARCHAR(60),
    ->     PRIMARY KEY(ID)
    -> );
```
#### Console output
```
Query OK, 0 rows affected (0.01 sec)
```

### This query inserts records into movie_genre table.

```sql
mysql> SELECT * FROM movie_genres WHERE movieID=1;
```
#### Console output
```
+---------+-----------+
| movieID | genre     |
+---------+-----------+
|       1 | Adventure |
|       1 | Animation |
|       1 | Children  |
|       1 | Comedy    |
|       1 | Fantasy   |
+---------+-----------+
5 rows in set (0.00 sec)
```
#### Adding a genre to a movie
```sql
mysql> INSERT INTO movie_genres(movieID, genre) VALUES(1, 'Action');
```
#### Console output
```
Query OK, 1 row affected (0.00 sec)
```
#### Verification
```sql
mysql> SELECT * FROM movie_genres WHERE movieID=1;
```
#### Console output
```
+---------+-----------+
| movieID | genre     |
+---------+-----------+
|       1 | Adventure |
|       1 | Animation |
|       1 | Children  |
|       1 | Comedy    |
|       1 | Fantasy   |
|       1 | Action    |
+---------+-----------+
6 rows in set (0.00 sec)
```

### This statement updates records by changing the user name in user rated movies table.

```sql
mysql> SELECT *
    -> FROM user_ratedmovies
    -> WHERE userID = 75 AND movieID = 3;
```
#### Console output
```
+--------+---------+--------+----------+------------+-----------+-----------+-------------+-------------+
| userID | movieID | rating | date_day | date_month | date_year | date_hour | date_minute | date_second |
+--------+---------+--------+----------+------------+-----------+-----------+-------------+-------------+
|     75 |       3 |      1 |       29 |         10 |      2006 |        23 |          17 |          16 |
+--------+---------+--------+----------+------------+-----------+-----------+-------------+-------------+
1 row in set (0.00 sec)
```
#### Updating ratings for a particular movie by an user
```sql
mysql> UPDATE user_ratedmovies
    -> SET rating = 8
    -> WHERE userID = 75 AND movieID = 3;
```
#### Console output
```
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0
```
#### Verification
```sql
mysql> SELECT *  FROM user_ratedmovies WHERE userID = 75 AND movieID = 3;
```
#### Console output
```
+--------+---------+--------+----------+------------+-----------+-----------+-------------+-------------+
| userID | movieID | rating | date_day | date_month | date_year | date_hour | date_minute | date_second |
+--------+---------+--------+----------+------------+-----------+-----------+-------------+-------------+
|     75 |       3 |      8 |       29 |         10 |      2006 |        23 |          17 |          16 |
+--------+---------+--------+----------+------------+-----------+-----------+-------------+-------------+
1 row in set (0.00 sec)
```

### This is a trigger which sanitizes the dirty ratigs of the movie by settig them to default.

```sql
mysql> delimiter //
mysql> CREATE TRIGGER sanitize_ratings BEFORE UPDATE ON user_ratedmovies
    -> FOR EACH ROW
    -> BEGIN
    ->     IF NEW.rating < 0 THEN
    -> SET NEW.rating = 0;
    ->     ELSEIF NEW.rating > 10 THEN
    -> SET NEW.rating = 10;
    -> END IF;
    -> END;//
Query OK, 0 rows affected (0.01 sec)
mysql> delimiter ;
```
#### Data
```sql
mysql> SELECT * FROM user_ratedmovies WHERE userId = 2 AND movieID = 1;
```
#### Console output
```
+--------+---------+--------+----------+------------+-----------+-----------+-------------+-------------+
| userID | movieID | rating | date_day | date_month | date_year | date_hour | date_minute | date_second |
+--------+---------+--------+----------+------------+-----------+-----------+-------------+-------------+
|      2 |       1 |      5 |       12 |         12 |      1998 |        12 |          12 |          12 |
+--------+---------+--------+----------+------------+-----------+-----------+-------------+-------------+
1 row in set (0.00 sec)
```
#### Dirty data updation
```sql
mysql> UPDATE user_ratedmovies SET rating = 500 WHERE userId = 2 AND movieID = 1;
```
#### Console output
```
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0
```
#### Verification
```sql
mysql> SELECT * FROM user_ratedmovies WHERE userId = 2 AND movieID = 1;
```
#### Console output
```
+--------+---------+--------+----------+------------+-----------+-----------+-------------+-------------+
| userID | movieID | rating | date_day | date_month | date_year | date_hour | date_minute | date_second |
+--------+---------+--------+----------+------------+-----------+-----------+-------------+-------------+
|      2 |       1 |     10 |       12 |         12 |      1998 |        12 |          12 |          12 |
+--------+---------+--------+----------+------------+-----------+-----------+-------------+-------------+
1 row in set (0.00 sec)
```
#### Dirty data updation
```sql
mysql> UPDATE user_ratedmovies
    -> SET rating = -200
    -> WHERE userId=2 AND movieID=1;
Query OK, 0 rows affected (0.00 sec)
Rows matched: 1  Changed: 0  Warnings: 0
```
#### Verification
```sql
mysql> SELECT * FROM user_ratedmovies WHERE userId = 2 AND movieID = 1;
```
#### Console output
```
+--------+---------+--------+----------+------------+-----------+-----------+-------------+-------------+
| userID | movieID | rating | date_day | date_month | date_year | date_hour | date_minute | date_second |
+--------+---------+--------+----------+------------+-----------+-----------+-------------+-------------+
|      2 |       1 |      0 |       12 |         12 |      1998 |        12 |          12 |          12 |
+--------+---------+--------+----------+------------+-----------+-----------+-------------+-------------+
1 row in set (0.00 sec)
```

### This function calculates the age.

```sql
CREATE FUNCTION AGE(d DATE)
    RETURNS INT DETERMINISTIC
    RETURN YEAR(NOW()) - YEAR(d);
```
#### Usage
```sql
mysql> SELECT AGE(str_to_date(CONCAT(date_year,'-', date_month, '-', date_day), '%Y-%m-%d')) AS 'Age of the rating' FROM user_ratedmovies WHERE userId=2 AND movieID=1;
```
#### Console output
```
+-------------------+
| Age of the rating |
+-------------------+
|                21 |
+-------------------+
1 row in set (0.00 sec)
```
#### Verification
```sql
mysql> SELECT title, year, AGE(str_to_date(CONCAT(year, '-01-01'), '%Y-%m-%d')) AS 'Age' FROM movies WHERE movieId=1;
```
#### Console output
```
+-----------+------+------+
| title     | year | Age  |
+-----------+------+------+
| Toy story | 1995 |   24 |
+-----------+------+------+
1 row in set (0.00 sec)
```

### This query delete records which are older than a particular year (2012 in this example).

```sql
DELETE FROM user_ratedmovies_timestamps WHERE date_year < 2012;
```
#### Console output
```
Query OK, 420 rows affected (0.50 sec)
```

### This is a constraint violation, here `42069123` does not exist 

```sql
mysql> INSERT INTO movie_genres VALUES(42069123, 'SOFT');
```
#### Console output
```
ERROR 1452 (23000): Cannot add or update a child row: a foreign key constraint fails (`p2`.`movie_genres`, CONSTRAINT `movie_genres_ibfk_1` FOREIGN KEY (`movieID`) REFERENCES `movies` (`movieID`) ON DELETE CASCADE)
```

## Queries

### This query creates a view named comedy_movies from movies table and movie_genres tables.

```sql
mysql> CREATE VIEW comedy_movies
    -> AS SELECT M.title, M.year
    -> FROM movies M, movie_genres MG
    -> WHERE M.movieID = MG.movieID AND MG.genre = 'comedy';
Query OK, 0 rows affected (0.00 sec)

mysql> DESC comedy_movies;
```
#### Console output
```
+-------+--------------+------+-----+---------+-------+
| Field | Type         | Null | Key | Default | Extra |
+-------+--------------+------+-----+---------+-------+
| title | varchar(250) | NO   |     | NULL    |       |
| year  | smallint(6)  | NO   |     | NULL    |       |
+-------+--------------+------+-----+---------+-------+
2 rows in set (0.00 sec)
```

#### Usage
```sql
mysql> SELECT * FROM comedy_movies WHERE year=1994;
```
#### Console output
```
+----------------------------------------------------+------+
| title                                              | year |
+----------------------------------------------------+------+
| Il postino                                         | 1994 |
| Keiner liebt mich                                  | 1994 |
| Mute Witness                                       | 1994 |
| Clerks.                                            | 1994 |
| Don Juan DeMarco                                   | 1994 |
| Dumb & Dumber                                      | 1994 |
| Yin shi nan nu                                     | 1994 |
| Exit to Eden                                       | 1994 |
| Ed Wood                                            | 1994 |
| I.Q.                                               | 1994 |
| Junior                                             | 1994 |
| The Madness of King George                         | 1994 |
| Mixed Nuts                                         | 1994 |
| Milk Money                                         | 1994 |
| Nina Takes a Lover                                 | 1994 |
| Only You                                           | 1994 |
| Pulp Fiction                                       | 1994 |
| Prï¿½t-ï¿½-Porter                                  | 1994 |
| Trzy kolory: Bialy                                 | 1994 |
| Shallow Grave                                      | 1994 |
| Swimming with Sharks                               | 1994 |
| The Sum of Us                                      | 1994 |
| Muriel's Wedding                                   | 1994 |
| Ace Ventura: Pet Detective                         | 1994 |
| The Adventures of Priscilla, Queen of the Desert   | 1994 |
| Bullets Over Broadway                              | 1994 |
| Corrina, Corrina                                   | 1994 |
| Crooklyn                                           | 1994 |
| The Flintstones                                    | 1994 |
| Forrest Gump                                       | 1994 |
| Four Weddings and a Funeral                        | 1994 |
| I Like It Like That                                | 1994 |
| I Love Trouble                                     | 1994 |
| It Could Happen to You                             | 1994 |
| The Mask                                           | 1994 |
| Maverick                                           | 1994 |
| Naked Gun 33 1/3: The Final Insult                 | 1994 |
| The Paper                                          | 1994 |
| Reality Bites                                      | 1994 |
| Riï¿½hie Riï¿½h                                    | 1994 |
| Speechless                                         | 1994 |
| True Lies                                          | 1994 |
| A Low Down Dirty Shame                             | 1994 |
| Airheads                                           | 1994 |
| The Air Up There                                   | 1994 |
| Beverly Hills Cop III                              | 1994 |
| Cabin Boy                                          | 1994 |
| City Slickers II: The Legend of Curly's Gold       | 1994 |
| Clean Slate                                        | 1994 |
| Cops and Robbersons                                | 1994 |
| The Cowboy Way                                     | 1994 |
| The Favor                                          | 1994 |
| Fear of a Black Hat                                | 1994 |
| With Honors                                        | 1994 |
| Getting Even with Dad                              | 1994 |
| House Party 3                                      | 1994 |
| The Hudsucker Proxy                                | 1994 |
| I'll Do Anything                                   | 1994 |
| In the Army Now                                    | 1994 |
| The Inkwell                                        | 1994 |
| Jimmy Hollywood                                    | 1994 |
| Lightning Jack                                     | 1994 |
| What Happened Was...                               | 1994 |
| North                                              | 1994 |
| Radioland Murders                                  | 1994 |
| The Ref                                            | 1994 |
| Renaissance Man                                    | 1994 |
| The Road to Wellville                              | 1994 |
| Serial Mom                                         | 1994 |
| Threesome                                          | 1994 |
| Little Big League                                  | 1994 |
| Spanking the Monkey                                | 1994 |
| The Little Rascals                                 | 1994 |
| Okno v Parizh                                      | 1994 |
| Un indien dans la ville                            | 1994 |
| Dellamorte Dellamore                               | 1994 |
| Der bewegte Mann                                   | 1994 |
| The Search for One-eye Jimmy                       | 1994 |
| Grosse fatigue                                     | 1994 |
| Blank Check                                        | 1994 |
| D2: The Mighty Ducks                               | 1994 |
| The Return of Jafar                                | 1994 |
| Il mostro                                          | 1994 |
| Jui kuen II                                        | 1994 |
| Jui kuen II                                        | 1994 |
| Interview with the Vampire: The Vampire Chronicles | 1994 |
| Blankman                                           | 1994 |
| Baby's Day Out                                     | 1994 |
| Bar Girls                                          | 1994 |
| Wagons East                                        | 1994 |
| Monkey Trouble                                     | 1994 |
| The Stand                                          | 1994 |
| Little Giants                                      | 1994 |
| Camp Nowhere                                       | 1994 |
| Angie                                              | 1994 |
| PCU                                                | 1994 |
| It's Pat                                           | 1994 |
| Love Affair                                        | 1994 |
| There Goes My Baby                                 | 1994 |
| Guarding Tess                                      | 1994 |
| Trapped in Paradise                                | 1994 |
| Car 54, Where Are You?                             | 1994 |
| Sleep with Me                                      | 1994 |
| Police Academy: Mission to Moscow                  | 1994 |
| A Million to Juan                                  | 1994 |
| Major League II                                    | 1994 |
| The Chase                                          | 1994 |
| Heisei tanuki gassen pompoko                       | 1994 |
+----------------------------------------------------+------+
108 rows in set (0.01 sec)
```

### This query returns the records of all actors who had more than 2 roles in a single movie.

```sql
mysql> SELECT P.pname, M.title, COUNT(*)
    -> FROM (
    -> SELECT R.actorID, R.movieID
    ->     FROM movie_actors R
    ->     GROUP BY R.actorID, R.movieID
    -> HAVING COUNT(*) >= 2
    -> ) tbl, movie_actors R
    -> JOIN movies M
    ->     ON R.movieID = M.movieID
    -> JOIN people P
    ->     ON R.actorID = P.id
    -> WHERE P.id = tbl.actorID AND M.movieID = tbl.movieID
    -> GROUP BY P.pname, M.title;
```
#### Console output
```
+-------------+-----------+----------+
| pname       | title     | COUNT(*) |
+-------------+-----------+----------+
| Annie Potts | Toy story |        3 |
+-------------+-----------+----------+
1 row in set (0.07 sec)
```

### This query returns the years in which a movie received a rating of exactly 3 or 4 and the results are sorted in ascending order.

```sql
mysql> SELECT DISTINCT M.year
    -> FROM movies M
    -> INNER JOIN user_ratedmovies MR USING(movieID)
    ->     WHERE MR.rating IN (3, 4)
    -> ORDER BY M.year;
```
#### Console output
```
+------+
| year |
+------+
| 1903 |
| 1915 |
| 1916 |
| 1917 |
| 1918 |
| 1919 |
| 1920 |
| 1921 |
| 1922 |
| 1923 |
| 1924 |
| 1925 |
| 1926 |
| 1927 |
| 1928 |
| 1929 |
| 1930 |
| 1931 |
| 1932 |
| 1933 |
| .... |
| 1980 |
| 1981 |
| 1982 |
| 1983 |
| 1984 |
| 1985 |
| 1986 |
| 1987 |
| 1988 |
| 1989 |
| 1990 |
| 1991 |
| 1992 |
| 1993 |
| 1994 |
| 1995 |
| 1996 |
| 1997 |
| 1998 |
| 1999 |
| 2000 |
| 2001 |
| 2002 |
| 2003 |
| 2004 |
| 2005 |
| 2006 |
| 2007 |
| 2008 |
| 2009 |
| 2010 |
| 2011 |
+------+
98 rows in set (0.82 sec)
```

### Movies with more than 39 ratings sorted by highest rating to the lowest. This query returns the list of movies whose total number of ratings are more than 39 and the returned results are sorted from high to low. 

```sql
mysql> SELECT M.title FROM movies M
    ->     INNER JOIN (
    ->         SELECT MR.movieID, sum(MR.rating) AS rate
    ->         FROM user_ratedmovies MR
    ->         GROUP BY MR.movieID
    ->         HAVING count(distinct MR.userID, MR.movieID) > 39
    ->         ORDER BY sum(MR.rating) DESC
    ->         LIMIT 20
    ->     ) tbl
    ->     ON M.movieID = tbl.movieID
    ->     ORDER BY tbl.rate DESC;
```
#### Console output
```
+--------------------------------------------------------+
| title                                                  |
+--------------------------------------------------------+
| The Matrix                                             |
| Pulp Fiction                                           |
| The Lord of the Rings: The Fellowship of the Ring      |
| The Shawshank Redemption                               |
| Forrest Gump                                           |
| The Lord of the Rings: The Two Towers                  |
| Fight Club                                             |
| American Beauty                                        |
| The Lord of the Rings: The Return of the King          |
| The Silence of the Lambs                               |
| The Sixth Sense                                        |
| Star Wars                                              |
| Memento                                                |
| Raiders of the Lost Ark                                |
| Shrek                                                  |
| Star Wars: Episode V - The Empire Strikes Back         |
| Back to the Future                                     |
| The Usual Suspects                                     |
| Pirates of the Caribbean: The Curse of the Black Pearl |
| Gladiator                                              |
+--------------------------------------------------------+
20 rows in set (1.71 sec)
```

### This query returns the name of the highest-rated movie along with its title, year, rating and releasing country.

```sql
mysql> SELECT title, year, rating, country
    -> FROM movies M
    -> NATURAL JOIN user_ratedmovies
    -> NATURAL JOIN movie_countries
    -> WHERE rating = (
    ->     SELECT MAX(rating)
    ->     FROM user_ratedmovies
    -> );
```
#### Console output
```
+-----------+------+--------+---------+
| title     | year | rating | country |
+-----------+------+--------+---------+
| Toy story | 1995 |     10 | USA     |
+-----------+------+--------+---------+
1 row in set (0.46 sec)
```

### Some directors directed more than one movie. For all such directors, return the titles of all movies directed by them, along with the director name. Sort by director name, then movie title.

```sql
mysql> SELECT MD.directorName, COUNT(*)
    -> FROM Movies M1
    -> INNER JOIN movie_directors MD USING(movieID)
    -> GROUP BY MD.directorName
    -> HAVING COUNT(*) > 1
    -> ORDER BY MD.directorName;
```
#### Console output
```
+--------------------------------+----------+
| directorName                   | COUNT(*) |
+--------------------------------+----------+
| Aaron Norris                   |        3 |
| Abbas Kiarostami               |        2 |
| Abel Ferrara                   |        8 |
| Adam Bernstein                 |        2 |
| Adam Brooks                    |        2 |
| Adam Curtis                    |        2 |
| Adam Marcus                    |        4 |
| Adam McKay                     |        2 |
| Adam Rifkin                    |        2 |
| Adam Shankman                  |        7 |
| Adrian Lyne                    |        7 |
| Adrienne Shelly                |        2 |
| Agnès Jaoui                    |        2 |
| Agnès Varda                    |        5 |
| Agnieszka Holland              |        7 |
|          ...                   |       ...|
| Wolfgang Petersen              |        8 |
| Wolfgang Reitherman            |        6 |
| Woo-ping Yuen                  |        2 |
| Woody Allen                    |       40 |
| Yahoo Serious                  |        2 |
| Yasujiro Ozu                   |        7 |
| Ye Lou                         |        2 |
| Yimou Zhang                    |       10 |
| Yoji Yamada                    |        2 |
| Youngyooth Thongkonthun        |        2 |
| Yvan Attal                     |        2 |
| Yves Robert                    |        2 |
| Zack Snyder                    |        4 |
| Zak Penn                       |        2 |
| Zalman King                    |        4 |
| Zhuangzhuang Tian              |        2 |
| Zoe R. Cassavetes              |        2 |
+--------------------------------+----------+
1715 rows in set (0.03 sec)
```

### The query returns the Year when most of comedy movies were produced, and number of movies and their average rating.

```sql
mysql> SELECT M.year, count(MG.genre), avg(avg_rating)
    -> FROM movies M
    -> NATURAL JOIN movie_genres MG
    -> NATURAL JOIN (
    -> SELECT MR.movieID, avg(MR.rating) as avg_rating
    ->     FROM user_ratedmovies MR
    ->     GROUP BY MR.movieID
    -> ) tbl
    -> WHERE MG.genre = 'comedy'
    -> GROUP BY M.year
    -> ORDER BY M.year;
```
#### Console output
```
+------+-----------------+-----------------+
| year | count(MG.genre) | avg(avg_rating) |
+------+-----------------+-----------------+
| 1903 |               1 |      4.00000000 |
| 1917 |               1 |      4.16670000 |
| 1918 |               1 |      4.14290000 |
| 1920 |               1 |      2.50000000 |
| 1921 |               1 |      3.88890000 |
| 1922 |               1 |      1.00000000 |
| 1923 |               3 |      4.24920000 |
| 1924 |               1 |      4.37500000 |
| 1925 |               2 |      4.10090000 |
| 1926 |               3 |      3.83253333 |
| 1927 |               2 |      3.51665000 |
| 1928 |               6 |      3.53190000 |
| 1929 |               2 |      3.74845000 |
| 1930 |               3 |      4.24240000 |
| 1931 |               6 |      3.88168333 |
| .... |              .. |         ....    |
| 1988 |              78 |      3.16113462 |
| 1989 |              82 |      3.23225732 |
| 1990 |              67 |      3.16785373 |
| 1991 |              80 |      3.15689625 |
| 1992 |              75 |      3.24524400 |
| 1993 |              88 |      3.17111477 |
| 1994 |             107 |      3.09324953 |
| 1995 |              97 |      3.21226186 |
| 1996 |             127 |      3.13403071 |
| 1997 |             122 |      3.08417705 |
| 1998 |             131 |      3.26854656 |
| 1999 |             129 |      3.30430233 |
| 2000 |             148 |      3.33658649 |
| 2001 |             162 |      3.30355679 |
| 2002 |             140 |      3.25888643 |
| 2003 |             144 |      3.36208125 |
| 2004 |             144 |      3.29098403 |
| 2005 |             149 |      3.40279866 |
| 2006 |             151 |      3.32102252 |
| 2007 |             144 |      3.32191944 |
| 2008 |             111 |      3.30677748 |
| 2009 |               3 |      3.03570000 |
| 2010 |               1 |      2.66670000 |
+------+-----------------+-----------------+
94 rows in set (1.96 sec)
```

### The query Creates views for actors with highest rated movies , one for low rated movied and then using these views list the actors with no flop movies.

```sql
mysql> CREATE VIEW highrating AS
    ->     SELECT DISTINCT R.actorID
    ->     FROM movie_actors R
    ->     WHERE R.movieID in (
    ->         SELECT MR.movieID
    ->         FROM user_ratedmovies MR
    ->         GROUP BY MR.movieID
    ->         HAVING AVG(MR.rating) >= 3.5
    ->     );
```
#### Console output
```
Query OK, 0 rows affected (0.01 sec)
```

```sql
mysql> CREATE VIEW highrating AS
    ->     SELECT DISTINCT R.actorID
    ->     FROM movie_actors R
    ->     WHERE R.movieID in (
    ->         SELECT MR.movieID
    ->         FROM user_ratedmovies MR
    ->         GROUP BY MR.movieID
    ->         HAVING AVG(MR.rating) < 3.5
    ->     );
```
#### Console output
```
Query OK, 0 rows affected (0.00 sec)
```
```sql
mysql> CREATE VIEW noFlop AS
    ->     SELECT *
    ->     FROM highrating h
    ->     WHERE h.actorID NOT IN (
    ->         SELECT l.actorID
    ->         FROM lowrating l
    ->     );
```
#### Console output
```
Query OK, 0 rows affected (0.00 sec)
```
```sql
mysql> SELECT P.pname, count(R.movieID) AS movie
    -> FROM noFlop NF, people P, movie_actors R
    -> WHERE P.ID = NF.actorID AND R.actorID = P.ID
    -> GROUP BY NF.actorID
    -> HAVING COUNT(*) >= 10
    -> ORDER BY movie DESC, P.pname ASC;
```
#### Console output
```
+-------------------+-------+
| pname             | movie |
+-------------------+-------+
| Harry Hayden      |    17 |
| Daniel Auteuil    |    16 |
| Leonard Mudie     |    15 |
| William Powell    |    14 |
| Cyril Ring        |    13 |
| Eugene Borden     |    13 |
| Wallis Clark      |    13 |
| Claire Du Brey    |    12 |
| Edward Gargan     |    12 |
| Gerald Sim        |    12 |
| Liv Ullmann       |    12 |
| Charles Sullivan  |    11 |
| Claudia Cardinale |    11 |
| Dexter Fletcher   |    11 |
| Don Beddoe        |    11 |
| Gino Corrado      |    11 |
| Henry Travers     |    11 |
| Hugh Griffith     |    11 |
| Jack Chefe        |    11 |
| Montgomery Clift  |    11 |
| Robert J. Wilke   |    11 |
| Ticky Holgado     |    11 |
| Tom Tyler         |    11 |
| Wolfgang Preiss   |    11 |
| Florence Bates    |    10 |
| Forbes Murray     |    10 |
| Halliwell Hobbes  |    10 |
| Howard Mitchell   |    10 |
| Lee Patrick       |    10 |
| Michael Elphick   |    10 |
| Michael Goodliffe |    10 |
| Miles Malleson    |    10 |
| Peter Lawford     |    10 |
| Robert Morley     |    10 |
+-------------------+-------+
34 rows in set (4.21 sec)
```
