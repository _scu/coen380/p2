# COEN380 Group Project 2

## Group 2

Ankur Dang | Ankush Chaknalwar | Mrithyunjay Halinge | Niral Shah | Sampath Shreekantha
---- | ---- | ---- | ---- | ----
1545115 | 1549325 | 1552192 | 1425877 | 1549494

## Project Overview
Each student will work within a group on a semester long two programming projects (Project-2 & Project-3) that are Database centric. Please contact the instructor if you are having difficulty joining a group or if you have ideas that you would like to discuss/validate.
The main goal of Project-2 is take an idea (such as shopping cart Application – minimum 3 tables) from English description level to E-R diagram and expressing constraints to an RDBMS schema implementation including constraints to populating the database with enough information to be able to:
1. Query the database with simple and complex operations (i.e., 2- or 3-way Join, nested queries, aggregation, Group-by, etc.).
1. Insert/Update/Delete operations as well.
1. Create a View and perform an insert/update on the View (trigger)
1. Starting a transaction and perform an operation that violates a constraint and demonstrate that you extended the schema so SQL runtime can maintain database consistency.
1. ....

Each group will need to turn in (~3-pages) proposal for the instructor feedback that describes the project idea, E-R diagram, and list of queries and IUDs you are planning to implement.
Each group will submit Final project (~10 pages Word Document) incorporating the instructor feedback on the proposal and screen shot of the queries and their results, etc.
Each team should meet and discuss possible ideas for the project. As a way of examples, please refer to the following web page for application examples: http://www.lovelycoding.org/2013/11/top-18-database-projects-ideas-for.engineering-bca-mca-btech-bsc.html

## Overview
In this project, use JDBC application to access/manipulate an installed RDBMS (you need to have an account), you will need to (**Programming Hints**):
1. Creating the database is typically done in a SQL script (i.e., books.sql) which would be executed using the specific database tool (e.g., for Oracle it is SQL*PLUS). As an alternative you can use JDBC instead.
1. Be careful when adding/deleting/updating a row in a table for constraint violation and address them.
1. Remember to specify which Table/attributes that are not allowed to be NULL?
1. Include one/two pages description of your project implementation. Remember to have good comments in your code. Also, include a hard copy of the java printout of the select statement results (cut-and paste from the screen).
1. Bring to class one complete hard-copy set of your project to the instructor, and send soft copy of the project to both (grader and instructor).