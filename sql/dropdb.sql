USE p2;

-- Drop Indices

DROP INDEX idx_movie_actors ON movie_actors;
DROP INDEX idx_movie_country ON movie_countries;
DROP INDEX idx_movie_year ON movies;
DROP INDEX idx_genre ON movie_genres;

-- Drop Tables
DROP TABLE user_ratedmovies;
DROP TABLE movie_locations;
DROP TABLE movie_countries;
DROP TABLE movie_actors;
DROP TABLE movie_directors;
DROP TABLE movie_genres;
DROP TABLE people;
DROP TABLE movies;