USE p1;

-- Show all populated data
SELECT * FROM movies;
SELECT * FROM people;
SELECT * FROM movie_genre;
SELECT * FROM roles;
SELECT * FROM imdb_user;
SELECT * FROM reviews;

-- Count the rows in each table
SELECT COUNT(*) FROM movies;
SELECT COUNT(*) FROM people;
SELECT COUNT(*) FROM movie_genre;
SELECT COUNT(*) FROM roles;
SELECT COUNT(*) FROM imdb_user;
SELECT COUNT(*) FROM reviews;

-- Simple select with ascending sort on movie title
SELECT *
FROM movies M
ORDER BY M.title ASC;

-- Simple select with descending sort on people name
SELECT *
FROM people P
ORDER BY P.name DESC;

-- People who acted in "Lucy"
SELECT P.name
FROM movies M, people P, roles R
WHERE M.movie_id = R.movie_id AND P.person_id = R.person_id AND M.title = "Lucy"
ORDER BY P.name ASC;

-- People who had more than 5 roles in a single movie
SELECT P.name, M.title as movie, count(DISTINCT R.Role)
FROM roles R, people P, movies M
WHERE M.movie_id = R.movie_id AND P.person_id = R.person_id
GROUP BY P.name, M.Title
HAVING count(DISTINCT R.Role) >= 5;

-- People with number of movies they have directed
SELECT P.name, count(M.title)
FROM people P
LEFT JOIN movies M
ON P.person_id = M.director_id
GROUP BY P.name;

-- People who have directed at least one movie with number of movies they have directed
SELECT P.name, count(M.title)
FROM people P
RIGHT JOIN movies M
ON P.person_id = M.director_id
GROUP BY P.name;

-- People who have acted in Action movies and the count
SELECT P.name, count(DISTINCT M.title) as "Number of Movies"
FROM people P
JOIN roles R
ON P.person_id = R.person_id
JOIN movies M
ON R.movie_id = M.movie_id
LEFT OUTER JOIN movie_genre MG
ON MG.movie_id = M.movie_id
WHERE MG.genre = "action"
GROUP BY P.name;

-- Highest rated movie every year after 2005
SELECT DISTINCT s.release_year, M2.title, s.rating
FROM (
  SELECT M1.release_year, max(R1.rating) as rating
    FROM movies M1, reviews R1
    WHERE M1.movie_id = R1.movie_id AND M1.release_year >= 2005
    GROUP BY M1.release_year
  ) s, movies M2, reviews R2
WHERE M2.movie_id = R2.movie_id AND M2.release_year = s.release_year AND R2.rating = s.rating
ORDER BY s.release_year ASC, M2.title ASC;

-- Average age of cast members for the movies ordered by "youngest cast" movie
SELECT M.title, AVG(YEAR(NOW()) - YEAR(P.birthday)) as "Average_Age"
FROM people P, movies M, roles R
WHERE M.movie_id = R.movie_id AND P.person_id = R.person_id
GROUP BY M.title
ORDER BY Average_Age ASC;


