USE p2;

--- Movies
CREATE TABLE movies(
	movieID INT NOT NULL,
	title VARCHAR(250) NOT NULL,
	imdbID VARCHAR(7) NOT NULL,
	spanishTitle VARCHAR(250),
	imdbPictureURL VARCHAR(300),
	year SMALLINT NOT NULL,
	rtID VARCHAR(100),
	rtAllCriticsRating SMALLINT,
	rtAllCriticsNumReviews SMALLINT,
	rtAllCriticsNumFresh SMALLINT,
	rtAllCriticsNumRotten SMALLINT,
	rtAllCriticsScore SMALLINT,
	rtTopCriticsRating INT,
	rtTopCriticsNumReviews SMALLINT,
	rtTopCriticsNumFresh SMALLINT,
	rtTopCriticsNumRotten SMALLINT,
	rtTopCriticsScore SMALLINT,
	rtAudienceRating INT,
	rtAudienceNumRatings INT,
	rtAudienceScore SMALLINT,
	rtPictureURL VARCHAR(300),
	PRIMARY KEY(movieID)
);

CREATE TABLE people(
	ID VARCHAR(60) NOT NULL,
	pname VARCHAR(60),
	PRIMARY KEY(ID)
);

CREATE TABLE movie_genres(
	movieID INT NOT NULL,
	genre VARCHAR(60) NOT NULL,
	FOREIGN KEY(movieID) REFERENCES Movies(movieID) ON DELETE CASCADE
);

CREATE TABLE movie_directors(
	movieID INT NOT NULL,
	directorID VARCHAR(60) NOT NULL,
	directorName VARCHAR(60),
	PRIMARY KEY(directorID, movieID),
	FOREIGN KEY(movieID) REFERENCES Movies(movieID) ON DELETE CASCADE
);

CREATE TABLE movie_actors(
	movieID INT NOT NULL,
	actorID VARCHAR(60) NOT NULL,
	actorName VARCHAR(60),
	ranking SMALLINT NOT NULL,
	PRIMARY KEY(actorID, movieID, actorName),
    FOREIGN KEY(actorID) REFERENCES people(ID) ON DELETE CASCADE,
	FOREIGN KEY(movieID) REFERENCES Movies(movieID) ON DELETE CASCADE
);

CREATE TABLE movie_countries(
	movieID INT NOT NULL,
	country VARCHAR(50),
	FOREIGN KEY(movieID) REFERENCES Movies(movieID) ON DELETE CASCADE
);

CREATE TABLE movie_locations(
	movieID INT NOT NULL,
	location1 VARCHAR(25),
	location2 VARCHAR(50),
	location3 VARCHAR(100),
	location4 VARCHAR(200),
	FOREIGN KEY(movieID) REFERENCES Movies(movieID) ON DELETE CASCADE
);


---- Ratings
CREATE TABLE user_ratedmovies(
	userID INT NOT NULL,
	movieID INT NOT NULL,
	rating SMALLINT NOT NULL,
	date_day SMALLINT NOT NULL,
	date_month SMALLINT NOT NULL,
	date_year SMALLINT NOT NULL,
	date_hour SMALLINT NOT NULL,
	date_minute SMALLINT NOT NULL,
	date_second SMALLINT NOT NULL,
	FOREIGN KEY(movieID) REFERENCES Movies(movieID) ON DELETE CASCADE
);

-- Create Indices

CREATE INDEX idx_genre ON movie_genres(genre);
CREATE INDEX idx_movie_year ON movies(year);
CREATE INDEX idx_movie_country ON movie_countries(country);
CREATE INDEX idx_movie_actors ON movie_actors(actorName);