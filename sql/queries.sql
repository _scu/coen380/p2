CREATE FUNCTION AGE(d DATE)
    RETURNS INT DETERMINISTIC
    RETURN YEAR(NOW()) - YEAR(d);


CREATE VIEW comedy_movies
AS SELECT M.title
FROM movies M, movie_genres MG
WHERE M.movieID = MG.movieID AND MG.genre = 'comedy';


SELECT P.pname, M.title, COUNT(*)
FROM (
	SELECT R.actorID, R.movieID
    FROM movie_actors R
    GROUP BY R.actorID, R.movieID
	HAVING COUNT(*) >= 2
) tbl, movie_actors R
JOIN movies M
    ON R.movieID = M.movieID
JOIN people P
    ON R.actorID = P.id
WHERE P.id = tbl.actorID AND M.movieID = tbl.movieID
GROUP BY P.pname, M.title;

INSERT into movie_actors(movieId, actorId, actorName, ranking) values(1, 'annie_potts', 'Annie Potts3', 5);


SELECT DISTINCT M.year
FROM movies M
INNER JOIN user_ratedmovies MR USING(MR.movieID)
    WHERE MR.rating IN (3, 4)
ORDER BY M.year;


SELECT M.title FROM movies M
    INNER JOIN (
        SELECT MR.movieID, sum(MR.rating) AS rate
        FROM user_ratedmovies MR
        GROUP BY MR.movieID
        HAVING count(distinct(MR.userID, MR.movieID)) > 39
        ORDER BY sum(MR.rating) DESC
        LIMIT 20
    ) tbl
    ON M.movieID = tbl.movieID
    ORDER BY tbl.rate DESC;
	
	
SELECT title, year, rating, mov_rel_country
FROM movies M
NATURAL JOIN user_ratedmovies MR1
WHERE MR1.rating = (
    SELECT MAX(MR2.rating)
    FROM user_ratedmovies MR2
);


SELECT MD.directorName, COUNT(*)
FROM Movies M1
INNER JOIN movie_directors MD USING(movieID)
GROUP BY MD.directorName
HAVING COUNT(*) >= 1
ORDER BY MD.directorName;


SELECT M.year, MG.genre, count(MG.genre), avg(MR.rating)
FROM movies M
NATURAL JOIN movie_genres MG
NATURAL JOIN user_ratedmovies MR
WHERE MG.genre = 'comedy'
GROUP BY M.year, MG.genre;


CREATE VIEW highrating AS
    SELECT DISTINCT(P.pname)
    FROM people P
    WHERE P.ID in (
        SELECT R.actorID
        FROM movie_actors R
        WHERE R.movieID in (
            SELECT MR.movieID
            FROM user_ratedmovies MR
            WHERE MR.rating >= 8
        )
    );

CREATE VIEW lowrating AS
    SELECT DISTINCT (P.pname)
    FROM people P
    WHERE P.ID in (
        SELECT R.actorID
        FROM movie_actors R
        WHERE R.movieID in (
            SELECT MR.movieID
            FROM user_ratedmovies MR
            WHERE MR.rating < 8
        )
    );

CREATE VIEW noFlop AS
    SELECT * FROM (
        (  
            SELECT *
            FROM highrating
        ) MINUS (
            SELECT *
            FROM lowrating
        )
    );

SELECT *
FROM (
    SELECT NF.pname, count(R.movieID) AS movie
    FROM noFlop NF, movie_actors R, people P
    WHERE P.ID = R.actorID AND P.pname = NF.pname
    GROUP BY NF.pname
    ORDER BY movie DESC, NF.pname ASC)
WHERE ROWNUM <= 10;
