# Project 2: COEN380 (Advanced Database Systems)

## Team: Group #2

Ankur Dang | Ankush Chaknalwar | Mrithyunjay Halinge | Niral Shah | Sampath Shreekantha
---- | ---- | ---- | ---- | ----
1545115 | 1549325 | 1552192 | 1425877 | 1549494

## Description
IMDb (Internet Movie Database)/Rotten Tomatoes is an online database of information related to films, television programs and streaming content online – including cast, production crew and personal biographies, plot summaries, trivia, fan and critical reviews, and ratings. The database has various tables inluding movies, actors, genres, ratings, directors etc. The database also reduces search with the implementation of indexes. The main goal of this is to show implementation of queries of varied complexity.

## Dataset
[MovieLens + IMDb/Rotten Tomatoes](http://files.grouplens.org/datasets/hetrec2011/hetrec2011-movielens-2k-v2.zip)

This dataset is an extension of MovieLens10M dataset, published by [GroupLeans research group](http://www.grouplens.org).

It links the movies of MovieLens dataset with their corresponding web pages at [Internet Movie Database (IMDb)](http://www.imdb.com) and [Rotten Tomatoes](http://www.rottentomatoes.com) movie review systems.

From the original dataset, only those users with both rating and tagging information have been mantained.

The dataset is released in the framework of the 2nd International Workshop on Information [Heterogeneity and Fusion in Recommender Systems (HetRec 2011)](http://ir.ii.uam.es/hetrec2011) at the 5th ACM Conference on [Recommender Systems (RecSys 2011)](http://recsys.acm.org/2011)

This dataset was built by Iván Cantador with the collaboration of Alejandro Bellogín and Ignacio Fernández-Tobías, members of the Information Retrieval group at [Universidad Autonoma de Madrid](http://ir.ii.uam.es)

## EER Diagram

![EER Diagram](images/EER diagram.png)

## Schema
```sql
mysql> DESC movies;
+------------------------+--------------+------+-----+---------+-------+
| Field                  | Type         | Null | Key | Default | Extra |
+------------------------+--------------+------+-----+---------+-------+
| ID                     | int(11)      | NO   | PRI | NULL    |       |
| title                  | varchar(250) | NO   |     | NULL    |       |
| imdbID                 | varchar(7)   | NO   |     | NULL    |       |
| spanishTitle           | varchar(250) | YES  |     | NULL    |       |
| imdbPictureURL         | varchar(300) | YES  |     | NULL    |       |
| year                   | smallint(6)  | NO   | MUL | NULL    |       |
| rtID                   | varchar(100) | YES  |     | NULL    |       |
| rtAllCriticsRating     | smallint(6)  | YES  |     | NULL    |       |
| rtAllCriticsNumReviews | smallint(6)  | YES  |     | NULL    |       |
| rtAllCriticsNumFresh   | smallint(6)  | YES  |     | NULL    |       |
| rtAllCriticsNumRotten  | smallint(6)  | YES  |     | NULL    |       |
| rtAllCriticsScore      | smallint(6)  | YES  |     | NULL    |       |
| rtTopCriticsRating     | int(2)       | YES  |     | NULL    |       |
| rtTopCriticsNumReviews | smallint(6)  | YES  |     | NULL    |       |
| rtTopCriticsNumFresh   | smallint(6)  | YES  |     | NULL    |       |
| rtTopCriticsNumRotten  | smallint(6)  | YES  |     | NULL    |       |
| rtTopCriticsScore      | smallint(6)  | YES  |     | NULL    |       |
| rtAudienceRating       | int(2)       | YES  |     | NULL    |       |
| rtAudienceNumRatings   | smallint(6)  | YES  |     | NULL    |       |
| rtAudienceScore        | smallint(6)  | YES  |     | NULL    |       |
| rtPictureURL           | varchar(300) | YES  |     | NULL    |       |
+------------------------+--------------+------+-----+---------+-------+
21 rows in set (0.00 sec)

mysql> DESC movie_actors;
+-----------+-------------+------+-----+---------+-------+
| Field     | Type        | Null | Key | Default | Extra |
+-----------+-------------+------+-----+---------+-------+
| movieID   | int(11)     | NO   | PRI | NULL    |       |
| actorID   | varchar(60) | NO   | PRI | NULL    |       |
| actorName | varchar(60) | YES  | MUL | NULL    |       |
| ranking   | smallint(6) | NO   |     | NULL    |       |
+-----------+-------------+------+-----+---------+-------+
4 rows in set (0.00 sec)

mysql> DESC movie_directors;
+--------------+-------------+------+-----+---------+-------+
| Field        | Type        | Null | Key | Default | Extra |
+--------------+-------------+------+-----+---------+-------+
| movieID      | int(11)     | NO   | PRI | NULL    |       |
| directorID   | varchar(60) | NO   | PRI | NULL    |       |
| directorName | varchar(60) | NO   |     | NULL    |       |
+--------------+-------------+------+-----+---------+-------+
3 rows in set (0.00 sec)

mysql> DESC movie_genres;
+---------+-------------+------+-----+---------+-------+
| Field   | Type        | Null | Key | Default | Extra |
+---------+-------------+------+-----+---------+-------+
| movieID | int(11)     | NO   | MUL | NULL    |       |
| genre   | varchar(60) | NO   | MUL | NULL    |       |
+---------+-------------+------+-----+---------+-------+
2 rows in set (0.01 sec)

mysql> DESC movie_countries;
+---------+-------------+------+-----+---------+-------+
| Field   | Type        | Null | Key | Default | Extra |
+---------+-------------+------+-----+---------+-------+
| movieID | int(11)     | NO   | MUL | NULL    |       |
| country | varchar(50) | YES  | MUL | NULL    |       |
+---------+-------------+------+-----+---------+-------+
2 rows in set (0.00 sec)

mysql> DESC movie_locations;
+-----------+--------------+------+-----+---------+-------+
| Field     | Type         | Null | Key | Default | Extra |
+-----------+--------------+------+-----+---------+-------+
| movieID   | int(11)      | NO   | MUL | NULL    |       |
| location1 | varchar(25)  | YES  |     | NULL    |       |
| location2 | varchar(50)  | YES  |     | NULL    |       |
| location3 | varchar(100) | YES  |     | NULL    |       |
| location4 | varchar(200) | YES  |     | NULL    |       |
+-----------+--------------+------+-----+---------+-------+
5 rows in set (0.00 sec)

mysql> DESC movie_tags;
+-----------+-------------+------+-----+---------+-------+
| Field     | Type        | Null | Key | Default | Extra |
+-----------+-------------+------+-----+---------+-------+
| movieID   | int(11)     | NO   | MUL | NULL    |       |
| tagID     | int(11)     | NO   | MUL | NULL    |       |
| tagWeight | smallint(6) | NO   | MUL | NULL    |       |
+-----------+-------------+------+-----+---------+-------+
3 rows in set (0.00 sec)

mysql> DESC tags;
+-------+-------------+------+-----+---------+-------+
| Field | Type        | Null | Key | Default | Extra |
+-------+-------------+------+-----+---------+-------+
| ID    | int(11)     | NO   | PRI | NULL    |       |
| value | varchar(50) | NO   |     | NULL    |       |
+-------+-------------+------+-----+---------+-------+
2 rows in set (0.00 sec)

mysql> DESC user_ratedmovies;
+-------------+-------------+------+-----+---------+-------+
| Field       | Type        | Null | Key | Default | Extra |
+-------------+-------------+------+-----+---------+-------+
| userID      | int(11)     | NO   |     | NULL    |       |
| movieID     | int(11)     | NO   | MUL | NULL    |       |
| rating      | smallint(6) | NO   |     | NULL    |       |
| date_day    | smallint(6) | NO   |     | NULL    |       |
| date_month  | smallint(6) | NO   |     | NULL    |       |
| date_year   | smallint(6) | NO   |     | NULL    |       |
| date_hour   | smallint(6) | NO   |     | NULL    |       |
| date_minute | smallint(6) | NO   |     | NULL    |       |
| date_second | smallint(6) | NO   |     | NULL    |       |
+-------------+-------------+------+-----+---------+-------+
9 rows in set (0.01 sec)

mysql> DESC user_ratedmovies_timestamps;
+-----------+-------------+------+-----+---------+-------+
| Field     | Type        | Null | Key | Default | Extra |
+-----------+-------------+------+-----+---------+-------+
| userID    | int(11)     | NO   |     | NULL    |       |
| movieID   | int(11)     | NO   | MUL | NULL    |       |
| rating    | smallint(6) | NO   |     | NULL    |       |
| timestamp | int(11)     | NO   |     | NULL    |       |
+-----------+-------------+------+-----+---------+-------+
4 rows in set (0.00 sec)

mysql> DESC user_taggedmovies;
+-------------+-------------+------+-----+---------+-------+
| Field       | Type        | Null | Key | Default | Extra |
+-------------+-------------+------+-----+---------+-------+
| userID      | int(11)     | NO   |     | NULL    |       |
| movieID     | int(11)     | NO   | MUL | NULL    |       |
| tagID       | int(11)     | NO   | MUL | NULL    |       |
| date_day    | smallint(6) | NO   |     | NULL    |       |
| date_month  | smallint(6) | NO   |     | NULL    |       |
| date_year   | smallint(6) | NO   |     | NULL    |       |
| date_hour   | smallint(6) | NO   |     | NULL    |       |
| date_minute | smallint(6) | NO   |     | NULL    |       |
| date_second | smallint(6) | NO   |     | NULL    |       |
+-------------+-------------+------+-----+---------+-------+
9 rows in set (0.01 sec)

mysql> DESC user_taggedmovies_timestamps;
+-----------+---------+------+-----+---------+-------+
| Field     | Type    | Null | Key | Default | Extra |
+-----------+---------+------+-----+---------+-------+
| userID    | int(11) | NO   |     | NULL    |       |
| movieID   | int(11) | NO   | MUL | NULL    |       |
| tagID     | int(11) | NO   | MUL | NULL    |       |
| timestamp | int(11) | NO   |     | NULL    |       |
+-----------+---------+------+-----+---------+-------+
4 rows in set (0.00 sec)
```

## Insert/Update/Delete

### Create movie actors table

```sql
CREATE TABLE movie_actors(
    movie_id INT NOT NULL,
    actor_id VARCHAR(60) NOT NULL,
    actor_name VARCHAR(60),
    ranking SMALLINT NOT NULL,
    PRIMARY KEY(actor_id, movie_id),
    FOREIGN KEY(movie_id) REFERENCES movies(movie_id) ON DELETE CASCADE
);
```

### Insert records into movie genres table

```sql
INSERT INTO movie_genre(movie_id, genre) VALUES('M2', 'Action');
```

### Update records in user_ratedmovies table to change the user name
```sql
UPDATE user_ratedmovies
SET rating = 7
WHERE userID = 43 AND movieID = 68;
```

### Trigger to sanitize dirty ratings by defaulting them

```sql
CREATE TRIGGER sanitize_ratings BEFORE UPDATE ON movie_ratings
    FOR EACH ROW
    BEGIN
        IF NEW.rating < 0 THEN
            SET NEW.rating = 0;
        ELSEIF NEW.rating > 10 THEN
            SET NEW.rating = 10;
        END IF;
    END;
```

### Function to calculate age

```sql
CREATE FUNCTION AGE(d DATE)
    RETURNS INT DETERMINISTIC
    RETURN YEAR(NOW()) - YEAR(d);
```

### Delete old user reviews (Older than 2012 in this example)

```sql
DELETE FROM user_ratedmovies_timestamps WHERE timestamp < 1315699200;
```

## Queries

### View for comedy movies

```sql
CREATE VIEW comedy_movies
AS SELECT M.title
FROM movies M, movie_genre MG
WHERE M.movie_id = MG.movie_id AND MG.genre = 'comedy';
```

### Actors having more than 2 roles

```sql
SELECT M.title, P.name, R.role
FROM movies M
JOIN roles R
    ON R.movie_id = M.movie_id
JOIN people P
    ON R.person_id = P.person_id
WHERE P.person_id IN (
    SELECT R.person_id
    FROM roles R
    GROUP BY R.person_id
    HAVING COUNT(*) >= 2
);
```

### All the years which produced a movie that received a rating of 3 or 4, and sort the result in increasing order

```sql
SELECT DISTINCT M.release_year
FROM movies M
INNER JOIN movie_ratings MR USING(MR.movie_id)
    WHERE MR.rev_stars IN (3, 4)
ORDER BY M.release_year;
```

### Movies with more than 39 ratings sorted by highest rating to the lowest

```sql
SELECT M.title FROM movies M
    INNER JOIN (
        SELECT MR.movie_id, sum(MR.rating) AS rate
        FROM movie_ratings MR
        GROUP BY MR.movie_id
        HAVING count(MR.review_id) > 39
        ORDER BY sum(MR.rating) DESC
        LIMIT 20
    ) tbl
    ON M.movie_id = tbl.movie_id
    ORDER BY tbl.rate DESC
```

### Highest-rated movie, and report its title, year, rating, and releasing country.

```sql
SELECT title, release_year, rev_stars, mov_rel_country
FROM movies M
NATURAL JOIN movie_ratings MR1
WHERE MR1.rev_stars = (
    SELECT MAX(MR2.rev_stars)
    FROM movie_ratings MR2
);
```

### Some directors directed more than one movie. For all such directors, return the titles of all movies directed by them, along with the director name. Sort by director name, then movie title.

```sql
SELECT M1.title, director
FROM Movie M1
INNER JOIN Movie M2 USING(director)
GROUP BY M1.movie_id
HAVING COUNT(*) > 1
ORDER BY director, M1.title;
```

### Year when most of comedy movies were produced, and number of movies and their average rating.

```sql
SELECT M.release_year, MG.genre, count(MG.genre), avg(MR.rev_stars)
FROM movies M
NATURAL JOIN movie_genres MG
NATURAL JOIN movie_ratings MR
WHERE MG.genre = 'comedy'
GROUP BY M.release_year, MG.genre;
```

### One view for highly rated movies, one view for low rated movies and using these views to list actors with no flop movies

```sql
CREATE VIEW highrating AS
    SELECT DISTINCT(P.name)
    FROM people P
    WHERE P.person_id in (
        SELECT R.person_id
        FROM roles R
        WHERE R.movie_id in (
            SELECT MR.movie_id
            FROM movie_reviews MR
            WHERE MR.rating >= 8
        )
    );

CREATE VIEW lowrating AS
    SELECT DISTINCT (P.name)
    FROM people P
    WHERE P.person_id in (
        SELECT R.person_id
        FROM roles R
        WHERE R.movie_id in (
            SELECT MR.movie_id
            FROM movie_reviews MR
            WHERE MR.rating < 8
        )
    );

CREATE VIEW noFlop AS
    SELECT * FROM (
        (  
            SELECT *
            FROM highrating
        ) MINUS (
            SELECT *
            FROM lowrating
        )
    );

SELECT *
FROM (
    SELECT NF.name, count(R.movie_id) AS movie
    FROM noFlop NF, roles R, people P
    WHERE P.person_id = R.person_id AND P.name = NF.name
    GROUP BY NF.name
    ORDER BY movie DESC, NF.name ASC)
WHERE ROWNUM <= 10;
```
