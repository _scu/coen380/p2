package com.mhalinge.scu.coen380.p2;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class populate {
    public static final List<String> fileList = Arrays.asList(
            "movies",
            "people",
            "movie_genres",
            "movie_directors",
            "movie_actors",
            "movie_countries",
            "movie_locations" ,
            "user_ratedmovies"
    );

    public static void main(String... args) {
        long startTime = new Date().getTime();

        SQLUtility sqlUtility = new SQLUtility();
        for(int i = fileList.size() - 1; i >= 0; i--) {
            sqlUtility.executeBatch(SQLStatementBuilder.delete(args[0], fileList.get(i)));
        }
        for(int i = 0; i < fileList.size(); i++) {
            sqlUtility.executeBatch(SQLStatementBuilder.insert(args[0], fileList.get(i)));
        }
        long time_taken = new Date().getTime() - startTime;
        System.out.println("Time taken: " + time_taken + " milliseconds");
    }
}

class SQLStatementBuilder {
    public static List<String> insert(String folderPath, String fileName) {
        String tableName = getTableName(fileName);
        List<String> lines = FileUtility.readFile(folderPath, fileName);
        List<String> sqlList = new ArrayList<>();

        StringBuilder stringBuilder;

        String tableHeader = String.join(", ", lines.get(0).split("\t"));

        List<String> ListData = lines.subList(1, lines.size());
        for(String data: ListData) {
            stringBuilder = new StringBuilder();

            stringBuilder.append("INSERT ").append("INTO ").append(tableName);
            stringBuilder.append("(").append(tableHeader).append(") ");
            stringBuilder.append("VALUES ");

            String[] dataItems = data.split("\t");
            if(dataItems.length != lines.get(0).split("\t").length) {
                dataItems = new String[lines.get(0).split("\t").length];

                dataItems[0] = data.split("\t")[0];
                for(int i = 1; i < dataItems.length; i++) {
                    dataItems[i] = "";
                }
            }
            for(int i = 0; i < dataItems.length; i++) {
                try {
                    if("\\N".equalsIgnoreCase(dataItems[i])) {
                        dataItems[i] = null;
                    } else if("infinity".equalsIgnoreCase(dataItems[i]) || "".equalsIgnoreCase(dataItems[i])) {
                        throw new Exception();
                    } else {
                        Float.parseFloat(dataItems[i]);
                    }
                } catch (Exception e) {
                    dataItems[i] = "'" + dataItems[i].replace("'", "''") + "'";
                }
            }
            stringBuilder.append("(").append(String.join(", ", dataItems)).append(");");

            sqlList.add(stringBuilder.toString());
        }

        return sqlList;
    }

    public static String delete(String folderPath, String fileName) {
        StringBuilder stringBuilder = new StringBuilder();
        String tableName = getTableName(fileName);

        stringBuilder.append("DELETE FROM ").append(tableName);

        return stringBuilder.toString();
    }

    private static String getTableName(String fileName) {
        return fileName.replace("-", "_");
    }
}

class FileUtility {
    public static List<String> readFile(String folderPath, String fileName) {
        List<String> lines = new ArrayList<>();
        File file = new File(fileName);

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(folderPath + File.separator + file + ".dat"), StandardCharsets.ISO_8859_1));

            String st;
            while ((st = br.readLine()) != null)
                lines.add(st);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }
}