package com.mhalinge.scu.coen380.p2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;

public class SQLUtility {
    private java.sql.Connection openConnection(
            final String host,
            final String port,
            final String dbName,

            final String userName,
            final String password

    ) throws SQLException {
        final String dbURL = "jdbc:mysql://" + host + ":" + port + "/" + dbName + "?useSSL=false&serverTimezone=UTC";
        return DriverManager.getConnection(dbURL, userName, password);
    }

    public Connection openConnection(

    ) throws SQLException {
        final String host = "localhost";
        final String port = "3306";

        final String dbName = "p2";

        final String userName = "root";
        final String password = "password";

        return this.openConnection(host, port, dbName, userName, password);
    }

    public void closeConnection(
            final Connection connection
    ) {
        try {
            connection.close();
        } catch (SQLException e) {
            System.err.println("Cannot close connection: " + e.getMessage());
        }
    }

    public void runQuery(
            final Connection connection,
            final String query
    ) {
        try {
            System.out.println(query + "\n");

            final java.sql.ResultSet resultSet = connection.createStatement().executeQuery(query);

            System.out.println("===============\n");
            for (int col = 1; col <= resultSet.getMetaData().getColumnCount(); col++) {
                System.out.print(resultSet.getMetaData().getColumnName(col) + ", ");
            }
            System.out.println("\n---------------\n");

            while (resultSet.next()) {
                for (int col = 1; col <= resultSet.getMetaData().getColumnCount(); col++) {
                    System.out.print(resultSet.getObject(col) + ", ");
                }
                System.out.println();
            }
            System.out.println("===============\n");

        } catch (java.sql.SQLException e) {
            System.err.println("Error when communicating with the database server: " + e.getMessage());
            System.err.println(query);
        }
    }

    public void executeBatch(String sql) {
        this.executeBatch(java.util.Collections.singletonList(sql));
    }

    public void executeBatch(java.util.List<String> sqlList) {
        java.sql.Connection con = null;

        try {
            con = openConnection();
            Statement statement = con.createStatement();

            int sqlListSize = sqlList.size();

            for(int i = 0; i < sqlListSize; i++) {
//                System.out.println(sqlList.get(i));
                statement.addBatch(sqlList.get(i));

                if((i + 1) % 100000 == 0 || i == sqlListSize - 1) {
                    statement.executeLargeBatch();
                    statement.clearBatch();
                    System.out.println("Batch completed. Last statement: " + sqlList.get(i));

                    closeConnection(con);
                    con = openConnection();
                    statement = con.createStatement();
                }
            }
        } catch (java.sql.SQLException e) {
            System.err.println("Errors occurs when communicating with the database server: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if(con != null) {
                closeConnection(con);
            }
        }
    }
}
