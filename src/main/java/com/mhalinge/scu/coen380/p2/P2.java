package com.mhalinge.scu.coen380.p2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class P2 {
    public static void main(
        final String... args
    ) {
        final List<String> selectQueries = Arrays.asList(
                "SELECT AGE(str_to_date(CONCAT(date_year,'-', date_month, '-', date_day), '%Y-%m-%d')) AS 'Age of the rating'\n" +
                        "FROM user_ratedmovies\n" +
                        "WHERE userId=2 AND movieID=1;",
                "SELECT * FROM comedy_movies WHERE year=1994;",
                "SELECT P.pname, M.title, COUNT(*)\n" +
                        "FROM (\n" +
                        "SELECT R.actorID, R.movieID\n" +
                        "    FROM movie_actors R\n" +
                        "    GROUP BY R.actorID, R.movieID\n" +
                        "HAVING COUNT(*) >= 2\n" +
                        ") tbl, movie_actors R\n" +
                        "JOIN movies M\n" +
                        "    ON R.movieID = M.movieID\n" +
                        "JOIN people P\n" +
                        "    ON R.actorID = P.id\n" +
                        "WHERE P.id = tbl.actorID AND M.movieID = tbl.movieID\n" +
                        "GROUP BY P.pname, M.title;",
                "SELECT DISTINCT M.year\n" +
                        "FROM movies M\n" +
                        "INNER JOIN user_ratedmovies MR USING(movieID)\n" +
                        "    WHERE MR.rating IN (3, 4)\n" +
                        "ORDER BY M.year;",
                "SELECT M.title FROM movies M\n" +
                        "    INNER JOIN (\n" +
                        "        SELECT MR.movieID, sum(MR.rating) AS rate\n" +
                        "        FROM user_ratedmovies MR\n" +
                        "        GROUP BY MR.movieID\n" +
                        "        HAVING count(distinct MR.userID, MR.movieID) > 39\n" +
                        "        ORDER BY sum(MR.rating) DESC\n" +
                        "        LIMIT 20\n" +
                        "    ) tbl\n" +
                        "    ON M.movieID = tbl.movieID\n" +
                        "    ORDER BY tbl.rate DESC;",
                "SELECT title, year, rating, country\n" +
                        "FROM movies M\n" +
                        "NATURAL JOIN user_ratedmovies\n" +
                        "NATURAL JOIN movie_countries\n" +
                        "WHERE rating = (\n" +
                        "    SELECT MAX(rating)\n" +
                        "    FROM user_ratedmovies\n" +
                        ");",
                "SELECT MD.directorName, COUNT(*)\n" +
                        "FROM Movies M1\n" +
                        "INNER JOIN movie_directors MD USING(movieID)\n" +
                        "GROUP BY MD.directorName\n" +
                        "HAVING COUNT(*) > 1\n" +
                        "ORDER BY MD.directorName;",
                "SELECT M.year, count(MG.genre), avg(avg_rating)\n" +
                        "FROM movies M\n" +
                        "NATURAL JOIN movie_genres MG\n" +
                        "NATURAL JOIN (\n" +
                        "SELECT MR.movieID, avg(MR.rating) as avg_rating\n" +
                        "    FROM user_ratedmovies MR\n" +
                        "    GROUP BY MR.movieID\n" +
                        ") tbl\n" +
                        "WHERE MG.genre = 'comedy'\n" +
                        "GROUP BY M.year\n" +
                        "ORDER BY M.year;",
                "SELECT P.pname, count(R.movieID) AS movie\n" +
                        "FROM noFlop NF, people P, movie_actors R\n" +
                        "WHERE P.ID = NF.actorID AND R.actorID = P.ID\n" +
                        "GROUP BY NF.actorID\n" +
                        "HAVING COUNT(*) >= 10\n" +
                        "ORDER BY movie DESC, P.pname ASC;"
        );

        SQLUtility sqlUtility = new SQLUtility();

        try(
            final Connection connection = sqlUtility.openConnection()
        ) {
            selectQueries.forEach(query -> sqlUtility.runQuery(connection, query));

            System.out.println(String.format("Number of select queries executed: %s", selectQueries.size()));

            sqlUtility.closeConnection(connection);
        } catch (SQLException e) {
            System.err.println("Error when communicating with the database server: " + e.getMessage());
        }
    }

}
